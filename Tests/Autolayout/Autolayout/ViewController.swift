//
//  ViewController.swift
//  Autolayout
//
//  Created by Marco Martignone on 15/02/2016.
//  Copyright © 2016 Marco Martignone. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var centerConstraint: NSLayoutConstraint!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var surnameTextField: UITextField!
    @IBOutlet var redButton: UIButton!
    
    // Declaring center of the view
    var viewCenter: CGFloat?
    // Declaring confettiView from external library
    var confettiView: SAConfettiView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup confettiView
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.colors = [UIColor(red:0.95, green:0.40, blue:0.27, alpha:1.0),
            UIColor(red:1.00, green:0.78, blue:0.36, alpha:1.0),
            UIColor(red:0.48, green:0.78, blue:0.64, alpha:1.0),
            UIColor(red:0.30, green:0.76, blue:0.85, alpha:1.0),
            UIColor(red:0.58, green:0.39, blue:0.55, alpha:1.0)]
        confettiView.intensity = 0.5
        confettiView.type = .Diamond
        
        // Setup center of the view
        self.viewCenter = self.centerConstraint.constant
        // Just adding a bit of corner radius
        self.redButton.layer.cornerRadius = 3;
        
        // Subscribing to notifications: keyboard will appear, disappear and when the device rotate
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "deviceRotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Triggered while the keyboard is showing
    func keyboardWillShow(notification: NSNotification) {
        
        // Getting keyboard size
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue();
        
        // Move the center costraint to the new position
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.centerConstraint.constant = self.viewCenter! - keyboardFrame.size.height / 2.5
        });
    }
    
    // Triggered while the keyboard is hiding
    func keyboardWillHide(notification: NSNotification) {
        self.centerConstraint.constant = self.viewCenter!
    }
    
    // Touch the background to dismiss the keyboard and center the elements
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
        self.centerConstraint.constant = self.viewCenter!
    }

    // UITextFieldDelegate method called when the return button is pressed. The textField delegate is setted in the storyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.centerConstraint.constant = self.viewCenter!
        return true
    }
    
    // Triggered while the device is rotating
    func deviceRotated() {
        self.view.endEditing(true)
        self.centerConstraint.constant = self.viewCenter!
    }
    
    // Start confetti surprise
    func startConfetti() {
        view.addSubview(confettiView)
        confettiView.startConfetti()
    }
    
    // Sto confetti and removing view
    func stopConfetti() {
        confettiView.stopConfetti()
        confettiView.removeFromSuperview()
    }

    @IBAction func redButtonPressed(sender: UIButton)
    {
        // Strigger alertview if the textFields are empty otherwise show confetti 🎊
        if nameTextField.text == "" || surnameTextField.text == "" {
            let emptyFieldAlertView = UIAlertController(title: "🚨", message: "Fill the fields to trigger the red button", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Understood", style: UIAlertActionStyle.Destructive, handler: nil)
            emptyFieldAlertView.addAction(cancelAction)
            
            self.presentViewController(emptyFieldAlertView, animated: true, completion: nil)
        } else {
            
            self.startConfetti()
            _ = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector:  Selector("stopConfetti"), userInfo: nil, repeats: true)
        }
        // Dismiss keyboard to appreciate the confetti surprise 🎊
        self.view.endEditing(true)
    }
}


