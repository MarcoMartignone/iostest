//
//  ViewController.swift
//  AVCamera
//
//  Created by Marco Martignone on 16/02/2016.
//  Copyright © 2016 Marco Martignone. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class FaceViewController: UIViewController {
    
    var session:AVCaptureSession!
    var stillImageOutput:AVCaptureStillImageOutput!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var faceRectCALayer:CALayer!
    var metadataOutput:AVCaptureMetadataOutput!
    
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var flashView: UIView!
    
    // Handling AVSession in an other thread
    private var sessionQueue:dispatch_queue_t = dispatch_queue_create("com.yourtion.SwiftFaceDetection.session_access_queue", DISPATCH_QUEUE_SERIAL)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Starting AVFoundation setup
        setupAVCaputure()
        
        // Adding attributes to the UIButton created in the XIB
        cameraButton.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
        cameraButton.layer.borderWidth = 3
        cameraButton.layer.borderColor = UIColor(white: 1.0, alpha: 1.0).CGColor
        cameraButton.layer.cornerRadius = cameraButton.frame.size.height/2
        view.bringSubviewToFront(cameraButton)
        
        // flashView is a white view that appear for 0.2 seconds while you are taking a pic
        self.flashView.alpha = 0
        self.view.bringSubviewToFront(flashView)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        // Start session before the app goes in foreground
        if (!session.running) {
            session.startRunning()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        // Stop session when the app goes in background
        if (!session.running) {
            session.startRunning()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func setupAVCaputure() {
        
        // Initializing session
        session = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        // Loop through capture devices
        let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        var captureDevice: AVCaptureDevice?
        
        for device in videoDevices{
            let device = device as! AVCaptureDevice
            if device.position == AVCaptureDevicePosition.Front {
                captureDevice = device
            }
        }
        
        // Show label if there is no camera, for example in used in the simulator
        if captureDevice == nil {
            let label = UILabel()
            label.text = "NO CAMERA"
            label.font = UIFont(name: "HelveticaNeue-UltraLight", size: 36)
            label.textColor = UIColor.redColor()
            label.frame = view.frame
            label.textAlignment = NSTextAlignment.Center
            view.addSubview(label)
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            if session.canAddInput(input) {
                session.addInput(input)
            }
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            
            if session.canAddOutput(stillImageOutput) {
                session.addOutput(stillImageOutput)
            }
        } catch {
            return
        }
    
        // Setting camera preview
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewLayer.frame = view.bounds
        previewLayer.frame = self.view.bounds;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.connection.videoOrientation = .Portrait
        view.layer.addSublayer(previewLayer)
        
        // Setting output for face recognition
        let metadataOutput = AVCaptureMetadataOutput()
        metadataOutput.setMetadataObjectsDelegate(self, queue: self.sessionQueue)
        if session.canAddOutput(metadataOutput) {
            session.addOutput(metadataOutput)
        }
        metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeFace]
        if session.canAddOutput(metadataOutput) {
            session.addOutput(metadataOutput)
        }
        
        // Setting face recognition square
        faceRectCALayer = CALayer()
        faceRectCALayer.zPosition = 1
        faceRectCALayer.borderColor = UIColor.redColor().CGColor
        faceRectCALayer.borderWidth = 3.0
        faceRectCALayer.zPosition = 20
        faceRectCALayer.hidden = true
        previewLayer.addSublayer(faceRectCALayer)
    
        session.startRunning()
    }
    
    @IBAction func cameraButtonPressed(sender: UIButton) {
        
        // Play vibration feedback
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        // Flash the screen
        flashScreen()
        
        guard let connection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) else {
            return
        }
        connection.videoOrientation = .Portrait
        
        // Capturing image
        stillImageOutput.captureStillImageAsynchronouslyFromConnection(connection) { (sampleBuffer, error) -> Void in
            guard sampleBuffer != nil && error == nil else {
                return
            }
            
            // Grab data from sampleBuffer and converting them to image
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
            guard let image = UIImage(data: imageData) else {
                return
            }
            // Saving image to camera roll
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
    
    func flashScreen() {
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.flashView.alpha = 1
            }) { (Bool) -> Void in
            self.flashView.alpha = 0
        }
    }
}

extension FaceViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    // Handle face recognition
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        var faces = Array<(id:Int,frame:CGRect)>()
        
        // Collecting faces
        for metadataObject in metadataObjects as! [AVMetadataObject] {
            if metadataObject.type == AVMetadataObjectTypeFace {
                if let faceObject = metadataObject as? AVMetadataFaceObject {
                    let transformedMetadataObject = previewLayer.transformedMetadataObjectForMetadataObject(metadataObject)
                    let face:(id: Int, frame: CGRect) = (faceObject.faceID, transformedMetadataObject.bounds)
                    faces.append(face)
                }
            }
        }
        print("FACE",faces)
        
        // Showing redsquare around the face
        if (faces.count>0){
            self.setLayerHidden(false)
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.1)
            self.faceRectCALayer.frame = self.findMaxFaceRect(faces)
            CATransaction.commit()
        } else {
            self.setLayerHidden(true)
        }
    }
    
    func setLayerHidden(hidden:Bool) {
        if (self.faceRectCALayer.hidden != hidden){
            print("Hidden: ", hidden)
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                self.faceRectCALayer.hidden = hidden
            });
        }
    }
    
    // Adjusting size of the red square
    func findMaxFaceRect(faces:Array<(id:Int,frame:CGRect)>) -> CGRect {
        if (faces.count == 1) {
            return faces[0].frame
        }
        var maxFace = CGRect.zero
        var maxFace_size = maxFace.size.width + maxFace.size.height
        for face in faces {
            let face_size = face.frame.size.width + face.frame.size.height
            if (face_size > maxFace_size) {
                maxFace = face.frame
                maxFace_size = face_size
            }
        }
        return maxFace
    }
    
}
