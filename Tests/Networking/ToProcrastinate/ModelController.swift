//
//  ModelController.swift
//  ToProcrastinate
//
//  Created by Marco Martignone on 16/02/2016.
//  Copyright © 2016 Marco Martignone. All rights reserved.
//

import UIKit
import CoreData

class ModelController {
    
    class func jsonParser(json: NSArray) {
        
        // Going throug the json response
        for item in json {
            if item["done"] != nil && item["image"] != nil && item["task"] != nil {
                
                // Creating an instance of AppDelegate
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                // Retriving context from AppDelegate
                let managedObjectContext = appDelegate.managedObjectContext
                
                let task = NSEntityDescription.insertNewObjectForEntityForName("Task", inManagedObjectContext: managedObjectContext) as! Task
                
                // Assigning parsed json to Task object in Core Data
                task.done = item["done"] as? Int
                task.image = item["image"] as? String
                task.task = item["task"] as? String
                
                // Saving
                appDelegate.saveContext()
            }
        }
    }
    
}