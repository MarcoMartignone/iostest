//
//  ViewController.swift
//  ToProcrastinate
//
//  Created by Marco Martignone on 16/02/2016.
//  Copyright © 2016 Marco Martignone. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    // Array of task to display in the table view
    var taskArray: [Task] = []
    var toDoArray: [Task] = []
    var doneArray: [Task] = []
    var procrastinateArray: [Task] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting delegates
        tableView.delegate = self
        tableView.dataSource = self
        
        // Fetch data from Core Data
        fetchCoreData()
        
        // If Core Data in empty fetch data from the API
        if taskArray.count == 0 {
           fetchData() 
        }
        
        // Pull to reset the data
        let options = PullToRefreshOption()
        options.backgroundColor = UIColor.lightGrayColor()
        options.backgroundColor = UIColor.whiteColor()
        tableView.addPullToRefresh(options: options) { () -> () in
            // Cleaning database from current entities
            self.cleanDatabase()
            
            // Fetch data again from the server
            self.fetchData()
            
            self.tableView.stopPullToRefresh()
        }
    
        // Customizing color of segmented control
        UISegmentedControl.appearance().tintColor = UIColor.redColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Enum of the different network errors
    enum NetworkError: String, ErrorType {
        case RequestFailed = "Error: no data from the server"
        case JSONFailed = "Error: JSON parsing failed"
    }
    
    func fetchData() {
        let baseUrl = NSURL(string: "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json")
        let request = NSMutableURLRequest(URL: baseUrl!)
        
        // Start session to fetch data from API
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) -> Void in
            do {
                
                // Handling errors
                guard let data = data else {
                    throw NetworkError.RequestFailed
                }
                
                guard let json = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? NSArray else {
                    throw NetworkError.JSONFailed
                }
                
                // Call the data parser that saves the elements in Core Data
                ModelController.jsonParser(json)
                
                // Fetching elements from Core Data
                self.fetchCoreData()
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
                
            } catch {
                print("Error: \(error)")
            }
            
        }.resume()
    }
    
    func fetchCoreData() {
        let fetchRequest = NSFetchRequest(entityName: "Task")
        let appDelegate = (UIApplication.sharedApplication()).delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        
        // Populating taskArray (main array) with data and populating secondary arrays filtering from the main one.
        do {
            taskArray = try managedObjectContext.executeFetchRequest(fetchRequest) as! [Task]
            toDoArray = taskArray.filter({ $0.done == 0})
            doneArray = taskArray.filter({ $0.done != 0})
        } catch {
            return
        }
    }
    
    // Removing entities from the database
    func cleanDatabase() {
        self.procrastinateArray.removeAll()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Task")
        
        do {
            let objects = try context.executeFetchRequest(fetchRequest)
            
            for object in objects {
                context.deleteObject(object as! NSManagedObject)
            }
        } catch {
            return
        }
    }
    
    @IBAction func segmentedControlTapped(sender: UISegmentedControl) {
        self.tableView.reloadData()
    }
    
}

// MARK: - UITableViewDelegate
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    // Setting number of row in the table view based on the segmented control
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            return toDoArray.count
        case 1:
            return doneArray.count
        case 2:
            return procrastinateArray.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            let item = toDoArray[indexPath.row]
            cell.textLabel?.text = item.task
            return cell
        case 1:
            let item = doneArray[indexPath.row]
            cell.textLabel?.text = item.task
            return cell
        case 2:
            let item = procrastinateArray[indexPath.row]
            cell.textLabel?.text = item.task
        default:
            return cell
        }
        return cell
    }
    
    // Swipe the cell to reveal contestualized commands: ⛱ DONE, 🙃 PROCRASTINATE, 🗑 DELETE
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        // Available in ToDo ☠️ tab, allow to move the element in Procrastinate 🙃 tab
        let procrastinateAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "🙃") { (UITableViewRowAction, NSIndexPath) -> Void in
            let item = self.toDoArray[indexPath.row]
            self.procrastinateArray.append(item)
            self.toDoArray.removeAtIndex(indexPath.row)
            self.tableView.reloadData()
        }
        procrastinateAction.backgroundColor = UIColor.lightGrayColor()
        
        // Available in ToDo ☠️ and Procrastinate 🙃 tab, allow to move the element in Done ⛱ tab
        let doneAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "⛱") { (UITableViewRowAction, NSIndexPath) -> Void in
            if self.segmentedControl.selectedSegmentIndex == 0 {
                let item = self.toDoArray[indexPath.row]
                self.doneArray.append(item)
                self.toDoArray.removeAtIndex(indexPath.row)
                self.tableView.reloadData()
            } else if self.segmentedControl.selectedSegmentIndex == 2 {
                let item = self.procrastinateArray[indexPath.row]
                self.doneArray.append(item)
                self.procrastinateArray.removeAtIndex(indexPath.row)
                self.tableView.reloadData()
            }
        }
        doneAction.backgroundColor = UIColor.lightGrayColor()
        
        // Available in Done ⛱ tab, delete the element from the table view
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Destructive, title: "🗑") { (UITableViewRowAction, NSIndexPath) -> Void in
            self.doneArray.removeAtIndex(indexPath.row)
            self.tableView.reloadData()
        }
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            return [procrastinateAction, doneAction]
        case 1:
            return [deleteAction]
        case 2:
            return [doneAction]
        default:
            break
        }
        
        return [procrastinateAction, doneAction, deleteAction]
    }
}
